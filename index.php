<html>

<head>
    <style>
        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }

        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
        }

        .form-signin .checkbox {
            font-weight: normal;
        }

        .form-signin .form-control {
            position: relative;
            font-size: 16px;
            height: auto;
            padding: 10px;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        .form-signin .form-control:focus {
            z-index: 2;
        }

        .form-signin input[type="text"] {
            margin-bottom: -1px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }

        .account-wall {
            margin-top: 10%;

            padding: 40px 0px 20px 0px;
            background-color: #f7f7f7;
            -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
        }

        .login-title {
            color: #555;
            font-size: 18px;
            font-weight: 400;
            display: block;
        }

        .profile-img {
            width: 96px;
            height: 96px;
            margin: 0 auto 10px;
            display: block;
            -moz-border-radius: 50%;
            -webkit-border-radius: 50%;
            border-radius: 50%;
        }

        .need-help {
            margin-top: 10px;
        }

        .new-account {
            display: block;
            margin-top: 10px;
        }

        .nav.navbar-nav li a {
            color: white;
            font-family: cursive;
        }

        .nav.navbar-nav li a:hover {
            background-color: #116ba7;
        }

        .navbar-brand {
            color: white;
        }

        .navbar-brand:hover {
            background-color: #0e7ec9;
            color: white;
        }

        .footer {
            background: #616f7e;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 6%;
        }

        .footer ul {
            padding: 10px;
        }

        .footer ul li {
            color: white;
        }

        .footer ul li a {
            color: white;
        }
    </style>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body>



    <nav class="navbar " style="background-color:#0e7ec9">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CycleInfo</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="#"> About</a>
                </li>
                <li>
                    <a href="#">Contact Us</a>
                </li>
                <li>
                    <a href="#">Help</a>
                </li>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a href="#">Sign Up</a>
                </li>
            </ul>
        </div>
    </nav>



    <?php
        include 'dbc.php';







        session_start();
        $_SESSION['flg']=0;
        if(isset($_POST['buttonSignin']))
        {
            $_SESSION['flg']=1;


            $email = $_POST['email'];
            $pass =$_POST['pass'];

            $sql = "SELECT * FROM `user` WHERE email='$email' and password='$pass'";

            $result = $db->query($sql);

            $res = $result->fetch_assoc();

            if ($res) {
                $_SESSION['user'] = $res;
                header('Location: home.php');
            }
            else{
                echo "<div class=\"container\">
                        <div class=\"row\">
                            <div class=\"col-sm-6 col-md-4 col-md-offset-4\">
                                <h1 class=\"text-center login-title\" style=\"font-size: 30px;margin-top: 7%;\">Sign in Here</h1>
                                <div class=\"account-wall\">
                                    <img class=\"profile-img\" src=\"https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120\"
                                         alt=\"\">
                                    <form class=\"form-signin\" action=\"index.php\" method='post'>
                                        <input type=\"text\" class=\"form-control\" placeholder=\"Email\" required autofocus>
                                        <input type=\"password\" class=\"form-control\" placeholder=\"Password\" required>
                                        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\" name='buttonSignin' style=\"background-color:#0e7ec9; \">
                                            Sign in</button>
                    
                                        <a href=\"#\" class=\"pull-right need-help\">Need help? </a>
                                        <span class=\"clearfix\"></span>
                                    </form>
                                </div>
                                <a href=\"#\" class=\"text-center new-account\">Create an account </a>
                            </div>
                        </div>
                    </div>";
            }
        }
        else if(isset($_POST['logout']))
        {
            session_destroy();
            $_SESSION['flg']=0;
        }
        else{
            echo "<div class=\"container\">
                    <div class=\"row\">
                        <div class=\"col-sm-6 col-md-4 col-md-offset-4\">
                            <h1 class=\"text-center login-title\" style=\"font-size: 30px;margin-top: 7%;\">Sign in Here</h1>
                            <div class=\"account-wall\">
                                <img class=\"profile-img\" src=\"https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120\"
                                     alt=\"\">
                                <form class=\"form-signin\" action=\"index.php\" method='post'>
                                    <input type=\"text\" class=\"form-control\" placeholder=\"Email\" name=\"email\" required autofocus>
                                    <input type=\"password\" class=\"form-control\" placeholder=\"Password\" name=\"pass\" required>
                                    <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\" name='buttonSignin' style=\"background-color:#0e7ec9; \">
                                        Sign in</button>
                
                                    <a href=\"#\" class=\"pull-right need-help\">Need help? </a>
                                    <span class=\"clearfix\"></span>
                                </form>
                            </div>
                            <a href=\"#\" class=\"text-center new-account\">Create an account </a>
                        </div>
                    </div>
                </div>";
        }
    ?>



        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <ul class="list-unstyled list-inline ">
                            <li>
                                <a href="#">Read the blog</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Submit</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Links</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>