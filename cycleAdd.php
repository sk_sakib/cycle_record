<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Home</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">



        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <style>
            .form-signin {
                max-width: 330px;
                padding: 15px;
                margin: 0 auto;
            }

            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }

            .form-signin .checkbox {
                font-weight: normal;
            }

            .form-signin .form-control {
                position: relative;
                font-size: 16px;
                height: auto;
                padding: 10px;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
            }

            .form-signin .form-control:focus {
                z-index: 2;
            }

            #brand,#cn,#mn,#price,#date{
                margin-bottom: 5%;
                border-bottom-left-radius: 0;
                border-bottom-right-radius: 0;
                height: 20%;
            }


            .account-wall {
                margin-top: 5%;
                width: 100%;
                padding: 3% 0%;
                background-color: #f7f7f7;

            }

            .login-title {
                color: #555;
                font-size: 18px;
                font-weight: 400;
                display: block;
            }
            .nav.navbar-nav li a {
                color: white;
                font-family: cursive;
            }

            .nav.navbar-nav li a:hover{
                background-color:#116ba7;
            }
            .navbar-brand {
                color: white;
            }
            .navbar-brand:hover{
                background-color:#0e7ec9;
                color: white;
            }
            .footer {
                background:#616f7e;
                position: absolute;
                bottom: 0;
                width: 100%;
                height: 6%;
            }

            .footer ul {
                padding: 10px;
            }

            .footer ul li {
                color: white;
            }

            .footer ul li a {
                color: white;
            }
        </style>

    </head>

    <body>



        <nav class="navbar " style="background-color:#0e7ec9">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home.php">CycleInfo</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="#"> Profile</a>
                    </li>
                    <li>
                        <a href="#">My Cycles</a>
                    </li>
                    <li>
                        <a href="#">Buy Cycles</a>
                    </li>

                </ul>
                <form class="navbar-form navbar-left" action="/action_page.php">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search User" name="search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
                <form class="navbar-form navbar-right" method="post" action="index.php">
                    <button type="submit" class="btn btn-default" style="background: none;border: none;color: white;">Sign Out</button>
                </form>
            </div>
        </nav>



        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4 col-md-offset-4" style="width: 50%;margin-left: 25%;">
                    <h1 class="text-center login-title" style="font-size: 30;margin-top: 5%;">Add New Cycle</h1>
                    <div class="account-wall">

                        <form class="form-signin">
                            <label for="brand">Brand</label>
                            <input type="text" class="form-control"  id="brand" required autofocus>
                            <label for="cn">Chassis Number</label>
                            <input type="text" class="form-control" id="cn" required>
                            <label for="mn">Model Number</label>
                            <input type="text" class="form-control"  id="mn" required >
                            <label for="Price">Price</label>
                            <input type="text" class="form-control"  id="price" required >
                            <label for="date">Date</label>
                            <input type="date" class="form-control"  id="date" required >

                            <button class="btn btn-primary" type="submit" style="background-color:#0e7ec9;width: 35%;height: 4%;">
                                Add</button>


                            <span class="clearfix"></span>
                        </form>
                    </div>

                </div>
            </div>
        </div>













        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <ul class="list-unstyled list-inline ">
                            <li>
                                <a href="#">Read the blog</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Submit</a>
                            </li>
                            <li>|</li>
                            <li>
                                <a href="#">Links</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>




</body>

</html>