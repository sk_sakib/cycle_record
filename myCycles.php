<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


        <link rel="stylesheet" href="style5.css" type="text/css">

    </head>
    <body>
        <nav class="navbar " style="background-color:#0e7ec9">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="home.php">CycleInfo</a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="new.php"> Profile</a>
                    </li>
                    <li>
                        <a href="myCycles.php">My Cycles</a>
                    </li>
                    <li>
                        <a href="buyCycle.php">Buy Cycles</a>
                    </li>

                </ul>
                <form class="navbar-form navbar-left" action="/action_page.php">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder="Search User" name="search">
                    </div>
                    <button type="submit" class="btn btn-default">Search</button>
                </form>
                <form class="navbar-form navbar-right" method="post" action="index.php">
                    <button type="submit" class="btn btn-default" style="background: none;border: none;color: white;">Sign Out</button>
                </form>
            </div>
        </nav>

        <?php
            include 'dbc.php';

            session_start();

            if($_SESSION['flg']!=1) header('Location: index.php');

            $user = $_SESSION['user'];
            
            
            $nid=$user['nid'];
            
            $sql="Select * from cycle_owner natural join cycle_info where nid='$nid'";
            
            $result=$db->query($sql);
            
            $res=$result->fetch_assoc();
            
            if(!$res)
            {
                echo'
                    <div class="Heading">
                        <h1 style="margin-left: 40%;margin-top: 20%;">No Cycle Is Added Yet</h1>
                    </div>
                    ';
            }
            else
            {
                echo'<div class="Heading">

                        <h2>All Cycles</h2>
                    </div>
                    <a class="btn  btn-primary btn-lg" href="UserPanel.php">Back TO Home</a>
                    ';
            }
            while($res)
            {
                
                $chassis=$res["chassis_number"];
                $brand=$res["brand_name"];
                $model=$res["model_number"];
                $price=$res['price'];
                $date_of_sale=$res['date_of_sale'];
                $for_sale=$res['for_sale'];
                $filename="pictures/$chassis.jpg";
                if (file_exists($filename))
                {
                    echo'
                        
                    <div  id="inline" href="signin.html">
                    <div class ="one">
                        <img src="pictures/'.$chassis.'.jpg" style="width:150px;height:100px;">
                    </div>
                        <div  class="two">
                            <p>Brand:  '.$brand.'
                                <form action="cycleInfo.php" method="POST" class="a">
                                    <input type="submit" class="btn btn-primary btn-sm dtlbutton" value="view details" name="details">
                                    <input type="hidden" name = "chassis" value='.$chassis.'>
                                </form>
                            </p>
                            <p>Model:  '.$model.'</p>
                            <p>Chassis Number:  '.$chassis.'</p>
                            
                        </div>
                        
                    </div>
                    
                    ';
                }
                else
                {
                    echo'
                        
                    <div  id="inline" href="signin.html">
                    <div class ="one">
                        <img src="pictures/no_image.jpg" style="width:150px;height:100px;">
                    </div>
                        <div  class="two">
                            <p>Brand:  '.$brand.'
                                <form action="cycleInfo.php" method="POST" class="a">
                                    <input type="submit" class="btn btn-primary btn-sm dtlbutton" value="view details" name="details">
                                    <input type="hidden" name = "chassis" value='.$chassis.'>
                                </form>
                            </p>
                            <p>Model:  '.$model.'</p>
                            <p>Chassis Number:  '.$chassis.'</p>
                            
                        </div>
                        
                    </div>
                    
                    ';
                }
                $res=$result->fetch_assoc();
            }
             $db->close();
        ?>
        
        
        
    </body>
</html>
