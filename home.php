<?php

session_start();
if($_SESSION['flg']!=1) header('Location: index.php');
?>







<!DOCTYPE html>
<html lang="en">

<head>
    <title>Home</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="style5.css" type="text/css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
</head>

<body>



    <nav class="navbar " style="background-color:#0e7ec9">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CycleInfo</a>
            </div>
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="new.php"> Profile</a>
                </li>
                <li>
                    <a href="myCycles.php">My Cycles</a>
                </li>
                <li>
                    <a href="buyCycle.php">Buy Cycles</a>
                </li>
                
            </ul>
            <form class="navbar-form navbar-left" action="/action_page.php">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Search User" name="search">
                </div>
                <button type="submit" class="btn btn-default">Search</button>
            </form>
            <form class="navbar-form navbar-right" method="post" action="index.php">
                <button type="submit" class="btn btn-default" style="background: none;border: none;color: white;">Sign Out</button>
            </form>
        </div>
    </nav>




    <div class="container">
        <div class="row">
            <div id="custom-search-input" style="width: 60%;margin-left: 20%;margin-top: 2%;margin-bottom: 3.5%;;">
                <div class="input-group col-md-12">
                    <input type="text" class="  search-query form-control" placeholder="Chassis Number" />
                    <span class="input-group-btn">
                        <button class="btn" type="button">
                            <span class=" glyphicon glyphicon-search"></span>
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>






    <div class="container" style="width: 60%;">

        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <img src="slide_pic/bikes1.jpg" alt="Los Angeles" style="width:100%;">
                </div>

                <div class="item">
                    <img src="slide_pic/bikes2.jpg" alt="Chicago" style="width:100%;">
                </div>

                <div class="item">
                    <img src="slide_pic/bikes3.jpg" alt="New york" style="width:100%;">
                </div>

                <div class="item">
                    <img src="slide_pic/bikes4.jpg" alt="New york" style="width:100%;">
                </div>

                <div class="item">
                    <img src="slide_pic/bikes5.jpg" alt="New york" style="width:100%;">
                </div>
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>










    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-5">
                    <ul class="list-unstyled list-inline ">
                        <li>
                            <a href="#">Read the blog</a>
                        </li>
                        <li>|</li>
                        <li>
                            <a href="#">Submit</a>
                        </li>
                        <li>|</li>
                        <li>
                            <a href="#">Links</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>


            <h1>
        </h1>


</body>

</html>